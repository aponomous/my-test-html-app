import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { WebView } from 'react-native-webview';
import IndexHTML from './src/index.html';

export default () => {
  return <WebView
    originWhitelist={['*']}
    source={IndexHTML}
    style={{ marginTop: 60 }}
  />
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
